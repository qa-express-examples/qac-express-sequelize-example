const Sequelize = require('sequelize');

// Create sequalizer instance
const sequelize = new Sequelize(
    'test',                 // Database
    'root',                 // Username
    'OctoberIntake19!',     // Password
    {
        host: 'localhost',
        dialect: 'mysql'
    }
);

// Import Models
const Item = sequelize.import(__dirname + '/item-model');
const Location = sequelize.import(__dirname + '/location-model');

// Associations
// 1:1
Item.belongsTo(Location);
Location.hasOne(Item);

// // 1:Many
// Item.belongsTo(Location);
// Location.hasMany(Item);

// // Many:Many
// Item.belongsToMany(Location, {through: 'itemsLocations'});
// Location.belongsToMany(Item, {through: 'itemsLocations'});


// Sync models and add default data
sequelize.sync({ force: true }).then(() => {
    Location.create({ address: '123 Somewhere' });
    Item.create({ name: 'test', locationId: 1 });
});

// Export models
module.exports = {
    Item,
    Location
};
